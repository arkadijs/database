# Hackerboards Dataset

This is the primary dataset for [hackerboards.com](https://hackerboards.com/).

The dataset subdirectory contains one folder for each manifacturer with yaml files for
all the boards. The toplevel of the dataset directory also contains the yaml files with
global values like architectures.

## Using the utilities

The `hackerboard_utils` folder contains utilities for managing the yaml files.

```shell-session
Install the utilities
$ python3 setup.py install
```

### The bulk utility

The `hackerboards-bulk` utility reads the yaml dataset and allows filtering on any of the fields using
the `--select` option. It then allows to bulk change one or more other columns with the `--set` option.

```shell-session
$ cd dataset

$ hackerboards-bulk --select architecture==ARMv8 --select "memory_max>8G"
manufacturer      name         architecture    memory_max
----------------  -----------  --------------  ------------
Xunlong Software  Orange Pi 5  ARMv8           32.0 GiB
Pine64            QuartzPro64  ARMv8           16.0 GiB
Radxa             Rock 5B      ARMv8           16.0 GiB

$ hackerboards-bulk --select soc=="Rockchip RK3399" --set cluster1_count=2
Updated 25 boards:
manufacturer              name                    cluster1_count
------------------------  ----------------------  ----------------
Xunlong Software          Orange Pi RK3399        2 -> 2
Xunlong Software          Orange Pi 4             2 -> 2
ASUS                      Tinker Board 2          2 -> 2
....
```